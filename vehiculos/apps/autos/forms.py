from django import forms

from apps.autos.models import Autos

class AutosForm(forms.ModelForm):
	class Meta:
		model = Autos
		fields = [
		'modelo',
		'color',
		'matricula',
		'status',
		]