from django.db import models

# Create your models here.

class Autos(models.Model):
	id = models.AutoField(primary_key = True)
	modelo = models.CharField(max_length =  200)
	color = models.CharField(max_length =  200)
	matricula =models.CharField(max_length =  200)
	status =models.CharField(max_length =  6)
	#matricula =models.IntegerField()