from django.urls import path,include
from apps.autos.views import index,registrar,listar,editar,eliminar

urlpatterns = [
    path('',index,name="home"),
    path('index',index,name="index"),
    path('registrar',registrar,name="register"),
    path('listar',listar,name="listar"),
    path('editar/<int:id>',editar,name="editar"),
    path('eliminar/<int:id>',eliminar,name="eliminar"),
]


#from django.conf.urls import url
#from views import *
#
#urlpatterns = [
#	url(r'^$',home,name="index"),
#	#url(r'^registrar_auto/',registrarAuto,name="registrar_auto"),
#]