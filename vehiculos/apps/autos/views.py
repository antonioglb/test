from django.shortcuts import render,redirect

from apps.autos.models import Autos
from apps.autos.forms import AutosForm
#from .models import Autos
#from .forms import AutosForm
from django.http import HttpResponse


def index(request):
	return render(request,'index.html')
	#return HttpResponse("esta es primer mensaje")


def registrar(request):
	if request.method == 'POST':
		form=AutosForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/auto/listar')
	else:
		form=AutosForm()
		return render(request,'autos/registrar_auto.html',{'form':form})



def listar(request):
	autos = Autos.objects.all()
	data = { 'autos':autos }
	return render(request, 'autos/listar_auto.html',data)


def editar(request,id):
	auto = Autos.objects.get(id = id)
	if request.method == 'GET':
		form = AutosForm(instance = auto)
	else:
		form = AutosForm(request.POST, instance = auto)
		if form.is_valid():
			form.save()
		return redirect('/auto/listar')
	return render(request,'autos/editar_auto.html',{'form':form})


def eliminar(request,id):
	auto = Autos.objects.get(id = id)
	if request.method == 'POST':
		auto.delete()
		return redirect('/auto/listar')
	return render(request,'autos/eliminar_auto.html',{'auto':auto})




#def register(request):
#	if request.method == 'POST':
#		form=AutosForm(request.POST)
#		if(form.is_valid())
#			form.save()
#			return redirect('index')
#	else:
#		form.AutosForm()
#		return render(request,'autos/registrar_auto.html',{'form':form})